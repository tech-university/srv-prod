package marianotorres.servicios;

import com.mongodb.ConnectionString;

import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;
import org.bson.Document;

import javax.print.Doc;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class ServiciosService {
    static MongoCollection<Document> servicios;

    private static MongoCollection<Document>getServiciosCollection(){//obtener conexion mongoDb
        ConnectionString cs= new ConnectionString("mongodb://localhost:27017");//cadena de conexion
        MongoClientSettings settings = MongoClientSettings.builder()
                .applyConnectionString(cs)
                .retryWrites(true)
                .build();
        MongoClient mongo = MongoClients.create(settings);//se puede montar un mongo client
        MongoDatabase database= mongo.getDatabase("dbprod");//referencia a bd
        return  database.getCollection("servicios");//devolvera siempre una instancia
    }
    /*
    public static void insert(String servicio) throws Exception{
        Document doc = Document.parse(servicio);//convertir String que nos pasan a Document org.bson
        servicios = getServiciosCollection();//accede
        servicios.insertOne(doc);//inserta en esa coleccion
    }

     */

    public static void insert(String strservicios) throws Exception{
        servicios = getServiciosCollection();
        Document doc = Document.parse(strservicios);
        List<Document> lst =doc.getList("servicios", Document.class);
        if (lst == null){
            servicios.insertOne(doc);
        }else{
            servicios.insertMany(lst);
        }


    }

    public static List getAll(){
        servicios = getServiciosCollection();//obiente instancia de la conexion
        List list= new ArrayList();
        FindIterable<Document> iterDoc = servicios.find();//Coleccion de documentos
        Iterator it = iterDoc.iterator();//iterar sobre ellos
        while (it.hasNext()){
            list.add(it.next());//lo agrega
        }
        return list;
    }
    public static List getFiltrados(String filtro){
        servicios = getServiciosCollection();
        List lst = new ArrayList();
        Document docFiltro = Document.parse(filtro);
        FindIterable<Document> iterDoc = servicios.find(docFiltro);
        Iterator it = iterDoc.iterator();//iterar sobre ellos
        while (it.hasNext()){
            lst.add(it.next());//lo agrega
        }
        return lst;
    }

    public static void update(String filtro,String updates){
        servicios = getServiciosCollection();//obiente instancia de la conexion
        Document docFiltro= Document.parse(filtro);//se convierte en Document contiene el filtro
        Document doc= Document.parse(updates);////se convierte en Document contiene el documento
        servicios.updateOne(docFiltro,doc);//va a la bd , aplica un find docFiltro y asigna los valores que tenga el doc
    }

}
